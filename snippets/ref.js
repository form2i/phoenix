function findRelatedItems(arr, targetItem) {
    const relatedItems = []

    function findRelated(item) {
        if (relatedItems.includes(item)) {
            return
        }

        relatedItems.push(item)

        for (const otherItem of arr) {
            if (otherItem.refId === item.id) {
                findRelated(otherItem)
            }
        }
    }

    findRelated(targetItem)

    return relatedItems
}

const arr = [
    { id: 1 },
    { id: 2, refId: 1 },
    { id: 3, refId: 2 },
    { id: 4, refId: 1 },
    { id: 5 }
]

const targetItem = { id: 1 }

const relatedItems = findRelatedItems(arr, targetItem)
console.log(relatedItems)
