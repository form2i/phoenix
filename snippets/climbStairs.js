function climbStairs(n) {
    let a
    let b

    for (let i = 0; i < n; i++) {
        if (i === 0) {
            a = 1
            b = 1
        } else {
            [a, b] = [b, a + b]
        }
    }

    return a
}

for (let i = 1; i <= 16; i++) {
    console.log(climbStairs(i))
}


// 尾递归实现
function climbStairsTail(n, a = 1, b = 1) {
    if (n === 0) return a

    return climbStairsTail(n - 1, b, a + b)
}

for (let i = 1; i <= 16; i++) {
    console.log(climbStairsTail(i))
}
